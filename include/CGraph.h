#pragma once
#include "CQueueElement.h"
#include <queue>

class CGraph
{
	const int mTownCount;
	const int mRoadCount;
	const char mTypeCount;
	const char mMinTypeCount;
	std::vector<CNode*> mNet;
	std::queue<CQueueElement> mQueue;
public:
	/**
	 * \brief TownCount getter
	 * \return Number of towns in net
	 */
	int getTownCount() const;

	/**
	 * \brief RoadCount getter
	 * \return Number of roads in net
	 */
	int getRoadCount() const;

	/**
	 * \brief Type count getter
	 * \return number of good types in net
	 */
	char getTypeCount() const;

	/**
	 * \brief Net getter
	 * \return vector <CNode *> - towns in net
	 */
	const std::vector<CNode*> & getNet() const;


	/**
	 * \brief adds on i position new node
	 * \param i  position of new node
	 * \param newone new node to be added
	 */
	void newNode(int i, CNode * newone);
	
	/**
	 * \brief Constructor of graph
	 * \param town_count number of towns in net
	 * \param road_count number of roads in net
	 * \param type_count number of good types in net
	 * \param min_type_count number of minimum conut off good to be in every town
	 */
	CGraph(const int town_count, const int road_count, const char type_count, const char min_type_count);
	void queuePush(CQueueElement sub);
	
	/**
	 * \brief Breadth-first search of export for every town
	 */
	void exportBfs();

	/**
	 * \brief Prints solution
	 */
	void printSolution();
	
};

