#pragma once
#include <vector>


class CNode
{
	std::vector<CNode*> mNeighbors;
	std::vector<bool> mImport;
	int mPrice;
	char mGood;
	char mImportSize;
public:
	
	/**
	 * \brief Neighbors getter
	 * \return vector<CNode*> of Neighbors
	 */
	const std::vector<CNode*> & getNeighbors() const;

	/**
	 * Returns import, import[i] is true if good already is imported in the town, false otherwise
	 * \brief Import getter
	 * \return vector<bool> of import 
	 */
	const std::vector<bool> & getImport() const;
	
	/**
	 * \brief Import of i type of good
	 * \param i type of good to be checked
	 * \return true if good already is imported in the town, false otherwise
	 */
	bool getImport(int i) const;

	/**
	 * \brief Price getter
	 * \return Current price of import in the city
	 */
	int getPrice() const;

	/**
	 * \brief Import size getter
	 * \return Current import size in the city
	 */
	char getImportSize() const;

	/**
	 * \brief Good type getter
	 * \return Good of the city
	 */
	char getGood() const;

	/**
	 * \brief Imports good of type for given price 
	 * \param type Type of good to be imported
	 * \param price price of import
	 */
	void newImport(const char type, const int price);

	/**
	 * \brief Adds neighbor to the town
	 * \param neighbor New neigbor
	 */
	void newNeighbors(CNode * neighbor);

	/**
	 * \brief Constructor 
	 * \param type good type of the city
	 * \param type_count How many good types there are in the net
	 */
	CNode(const char type, const char type_count);
	
	/**
	 * Prints the node in format Price Good1 Good2 ..., where Price is import price of goods and Good1, ... are imported good types
	 * \brief prints the Node 
	 */
	void printNode() const;
};
