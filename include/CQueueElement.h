#pragma once
#include "CNode.h"

/**
 * \brief Element for queue
 * Algorithm needs in queue three informations - price of export, type of export and city to control
 */
struct CQueueElement
{

	const int m_Price;
	const char m_Type;
	const CNode* m_City;
	/**
	 * \brief Constructor
	 * \param price price of export
	 * \param type good type to be exported
	 * \param city city to control 
	 */
	CQueueElement(const int price, const char type, const CNode* city);
};
