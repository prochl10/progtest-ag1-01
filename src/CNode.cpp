#include "CNode.h"
#include <iostream>

CNode::CNode(const char type, const char type_count) : mPrice(0), mGood(type), mImportSize(1)
{
	mImport.resize(type_count, false);
	mImport[type] = true;
}

const std::vector<CNode*> & CNode::getNeighbors() const
{
	return mNeighbors;
}

const std::vector<bool> & CNode::getImport() const
{
	return mImport;
}

bool CNode::getImport(int i) const
{
	return mImport[i];
}

int CNode::getPrice() const
{
	return mPrice;
}

char CNode::getImportSize() const
{
	return mImportSize;
}

char CNode::getGood() const
{
	return mGood;
}

void CNode::newImport(const char type, const int price)
{
	mImport[type] = true;
	++mImportSize;
	mPrice += price + 1;
}

void CNode::newNeighbors(CNode * neighbor)
{
	mNeighbors.push_back(neighbor);
}

std::ostream & operator<<(std::ostream& lhs, const CNode& rhs)
{
	lhs << rhs.getPrice();
	for (int i = 0; i < static_cast<int>(rhs.getImport().size()); ++i)
	{
		if (rhs.getImport(i))
			lhs << " " << i;
	}
	lhs << std::endl;
	return lhs;
}

void CNode::printNode() const
{
	std::cout << *this;
}
