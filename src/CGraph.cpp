#include "CGraph.h"
#include <ostream>
#include <iostream>


int CGraph::getTownCount() const
{
	return mTownCount;
}

int CGraph::getRoadCount() const
{
	return mRoadCount;
}

char CGraph::getTypeCount() const
{
	return mTypeCount;
}

const std::vector<CNode*>& CGraph::getNet() const
{
	return mNet;
}

void CGraph::newNode(int i, CNode* newone)
{
	mNet[i] = newone;
}

CGraph::CGraph(const int town_count, const int road_count, const char type_count, const char min_type_count)
	:mTownCount(town_count), mRoadCount(road_count), mTypeCount(type_count), mMinTypeCount(min_type_count)
{
	mNet.resize(town_count);
}

void CGraph::queuePush(const CQueueElement sub)
{
	mQueue.push(sub);
}

void CGraph::exportBfs()
{
	while (!mQueue.empty())
	{
		const auto curr = mQueue.front();

		for (auto element : curr.m_City->getNeighbors())
		{
			if (element->getImportSize() < mMinTypeCount && !element->getImport()[curr.m_Type])
			{
				mQueue.push(CQueueElement(curr.m_Price + 1, curr.m_Type, element));
				element->newImport(curr.m_Type, curr.m_Price);
			}
		}
		mQueue.pop();
	}
}

void CGraph::printSolution()
{
	long long int total_price = 0;
	for (auto element : mNet)
	{
		total_price += element->getPrice();
	}
	std::cout << total_price << std::endl;
	for (auto element : mNet)
	{
		element->printNode();
	}
}