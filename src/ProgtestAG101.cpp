// ProgtestAG101.cpp : Lucie Procházková prochl10@fit.cvut.cz

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <vector>
#include <functional>
#include <queue>
#include "CNode.h"
#include "CQueueElement.h"
#include "CGraph.h"




int main()
{
	int counts[4];
	for (auto& count : counts)
		std::cin >> count;
	auto graph = new CGraph(counts[0], counts[1], counts[2], counts[3]);
	int type;
	for (auto i = 0; i < graph->getTownCount(); ++i)
	{
		std::cin >> type;
		graph->newNode(i,new CNode(type,graph->getTypeCount()));
		graph->queuePush(CQueueElement(0, type, graph->getNet()[i]));
	}
	int town_a, town_b;
	for (auto i = 0; i < graph->getRoadCount(); ++i)
	{
		std::cin >> town_a;
		std::cin >> town_b;
		graph->getNet()[town_a]->newNeighbors(graph->getNet()[town_b]);
		graph->getNet()[town_b]->newNeighbors(graph->getNet()[town_a]);
	}
	graph->exportBfs();
	graph->printSolution();
	return 0;
}