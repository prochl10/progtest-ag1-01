#
ProgtestAG101: build/main.o build/CNode.o build/CGraph.o build/CQueueElement.o 
	mkdir -p build
	g++ build/main.o build/CNode.o build/CGraph.o build/CQueueElement.o -o programm

build/main.o: src/ProgtestAG101.cpp include/CNode.h include/CGraph.h include/CQueueElement.h 
	mkdir -p build
	g++ -g -Wall -pedantic -std=c++11 -O2 -c src/ProgtestAG101.cpp -o build/main.o


build/CNode.o: src/CNode.cpp include/CNode.h 
	mkdir -p build
	g++ -g -Wall -pedantic -std=c++11 -O2 -c src/CNode.cpp -o build/CNode.o


build/CQueueElement.o: src/CQueueElement.cpp include/CQueueElement.h 
	mkdir -p build
	g++ -g -Wall -pedantic -std=c++11 -O2 -c src/CQueueElement.cpp -o build/CQueueElement.o

build/CGraph.o: src/CGraph.cpp include/CGraph.h
	mkdir -p build
	g++ -g -Wall -pedantic -std=c++11 -O2 -c src/CGraph.cpp -o build/CGraph.o


clean:
	rm complex
	rm -r build

.PHONY: clean
